import pygame as pg
from resources import R
from eventutils import PyGameBase
from pygame.math import Vector2 as V2


class Interface(PyGameBase):
    _sight = V2(640., 480.)
    _tracking = False
    _movement = False

    objects = [
        V2(-20, 30),
        V2(-100, -40),
        V2(70, 85),
        V2(120, 300),
        V2(250, -90),
    ]

    R = None

    position = V2(0.0, 0.0)
    target = V2(0.0, 0.0)
    target_loc = V2(0.0, 0.0)

    def __init__(self):
        PyGameBase.__init__(self, 30)
        self.dim = (640, 480)

    def init(self):
        self.window = pg.display.set_mode(
            self.dim, pg.RESIZABLE | pg.HWSURFACE | pg.DOUBLEBUF)
        pg.display.set_caption(R().strings['window_title'])

    def update(self):
        if self._movement:
            self.move()
        self.window.fill(R().colors['bg'])
        p_pos = (int(self.dim[0] / 2), int(self.dim[1] / 2))
        t_pos = [int(v) for v in V2(p_pos) + self.target - self.position]
        tl_pos = [int(v) for v in V2(p_pos) + self.target_loc - self.position]
        self._draw_objects(p_pos)
        self._draw_target_loc(tl_pos)
        self._draw_player(p_pos)
        if self._tracking:
            self._draw_target(t_pos)
            self._draw_targetting(p_pos, t_pos)
        self._draw_hud()

    def on_key_down(self, event):
        if event.unicode == 'q':
            self.stop()

    def on_mouse_down(self, event):
        if event.button == 1:
            self.set_target(event.pos)
            self._tracking = True

    def on_mouse_up(self, event):
        if event.button == 1:
            self.set_target_loc(event.pos)
            self._tracking = False
            self._movement = True

    def on_mouse_move(self, event):
        if event.buttons == (1, 0, 0):
            self.set_target(event.pos)

    def set_target(self, disp_pos):
        vec = V2(disp_pos) - V2([d / 2 for d in self.dim])
        self.target = self.position + vec

    def set_target_loc(self, disp_pos):
        vec = V2(disp_pos) - V2([d / 2 for d in self.dim])
        self.target_loc = self.position + vec

    def move(self):
        movement = 3.
        distance = self.position.distance_to(self.target_loc)
        if distance < .5:
            self._movement = False
            return
        if distance < movement:
            self.position = self.target_loc
            self._movement = False
            return
        else:
            vec = self.target_loc - self.position
            vec.scale_to_length(2.)
            self.position = self.position + vec

    def _draw_player(self, p_pos):
        image = R().images['player']
        self.window.blit(
            image, (
                p_pos[0] - int(image.get_width() / 2),
                p_pos[1] - image.get_height()
            )
        )
        pos_label = R().fonts['player'].render(
            'Kosa', True, R().colors['player_spot'])
        self.window.blit(
            pos_label, (
                p_pos[0] - image.get_width(),
                p_pos[1] - image.get_height() - 10
            )
        )

    def _draw_target(self, t_pos):
        color = R().colors['player_target']
        pg.draw.circle(self.window, color, t_pos, 3)

    def _draw_target_loc(self, t_pos):
        image = R().images['target']
        vec = [int(c / 2) for c in image.get_size()]
        self.window.blit(
            image, (t_pos[0] - vec[0], t_pos[1] - vec[1]))

    def _draw_targetting(self, p_pos, t_pos):
        color = R().colors['player_target']
        pg.draw.aaline(
            self.window, color, (p_pos[0], p_pos[1]), t_pos)
        pg.draw.circle(self.window, color, t_pos, 3)

    def _draw_hud(self):
        pos_label = R().fonts['hud'].render(
            '({:0.2f}, {:0.2f})'.format(*self.position), True,
            R().colors['player_spot'])
        self.window.blit(pos_label, (3, 3))

    def _draw_objects(self, p_pos):
        for obj in self.objects:
            coords = [int(x) for x in V2(p_pos) + obj - self.position]
            pg.draw.circle(self.window, R().colors['object'], coords, 4)
            label = R().fonts['player'].render(
                '({:0.2f},{:0.2f})'.format(*obj), True,
                R().colors['object'])
            self.window.blit(
                label, (coords[0] - 10, coords[1] + 6)
            )
