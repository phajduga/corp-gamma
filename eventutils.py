import pygame as pg


class StopPyGame(Exception):
    pass


class PyGameBase(object):
    _callbacks = {
        pg.MOUSEBUTTONDOWN: 'on_mouse_down',
        pg.MOUSEBUTTONUP: 'on_mouse_up',
        pg.MOUSEMOTION: 'on_mouse_move',
        pg.QUIT: 'on_quit',
        pg.VIDEORESIZE: 'on_resize',
        pg.KEYDOWN: 'on_key_down',
    }

    def __init__(self, fps):
        self.fps = fps
        self._fps_clock = pg.time.Clock()
        pg.init()

    def start(self):
        try:
            self.init()
            while True:
                for event in pg.event.get():
                    try:
                        event_name = self._callbacks[event.type]
                        evt = getattr(self, event_name, lambda evt: None)
                        evt(event)
                    except KeyError:
                        pass
                self.update()
                pg.display.flip()
                self._fps_clock.tick(self.fps)
        except StopPyGame:
            pass

    def init(self):
        pass

    def update(self):
        pass

    def on_quit(self, event):
        self.stop()

    def stop(self):
        raise StopPyGame()
