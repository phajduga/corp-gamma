import pygame as pg
import os.path


class Resources(object):
    base_path = 'resources/'
    strings = {
        'window_title': 'Corp Gamma MMORPG Pre-Alpha',
    }
    colors = {
        'bg': pg.Color('#aab1b2'),
        'player_spot': pg.Color('#55efff'),
        'player_target': pg.Color('#5de6ff'),
        'object': pg.Color('#e85072'),
    }
    fonts = {}
    images = {}

    def __init__(self):
        font_path = self.get_resource_path('fonts/Roboto-Medium.ttf')
        self.fonts['hud'] = pg.font.Font(font_path, 12)
        self.fonts['player'] = pg.font.Font(font_path, 9)
        img_path = self.get_resource_path('images/player.png')
        self.images['player'] = pg.image.load(img_path)
        img_path = self.get_resource_path('images/target.png')
        self.images['target'] = pg.image.load(img_path)

    def get_resource_path(self, relpath):
        return os.path.join(self.base_path, relpath)


_resources = None


def R():
    global _resources
    if _resources is None:
        _resources = Resources()
    return _resources
